interface rr_arb_if # (
    parameter P_PORT_N = 2,
    localparam int C_LOG2_PORTS = $clog2(P_PORT_N)
) (
    input clk_i
);

logic [P_PORT_N - 1 : 0] request_i, grant_o;
logic valid_o;
logic ack_i;
logic rst_n_i;
logic [C_LOG2_PORTS - 1:0] select_o;

modport DUT (
input  request_i,
output grant_o,
output valid_o,
output select_o,
input  ack_i,
input  clk_i,
input  rst_n_i
);

modport TEST (
output request_i,
input  grant_o,
input  valid_o,
input  select_o,
output ack_i,
input  clk_i,
input  rst_n_i
);

modport MONITOR (
input  request_i,
input  grant_o,
input  valid_o,
input  select_o,
input  ack_i,
input  clk_i,
input  rst_n_i
);

endinterface : rr_arb_if