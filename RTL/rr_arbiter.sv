//
// File : rr_arbiter.sv
//
// Created:
//          by HDL Designers Team
//          of Electronics Design Center "OhT"
//          <hdlhouse.com: http://www.hdlhouse.com>
//
//
// File Description:
//
// Round-Robin arbiter module
//

module rr_arbiter #(
    localparam int P_DEF_WEIGHT = 0,
    parameter int P_PORT_N = 4,
    int P_WEIGHTS[P_PORT_N] = '{default: P_DEF_WEIGHT},
    int P_LOG2_MAX_WEIGHT_P1 = $clog2(P_DEF_WEIGHT + 1),
    int P_INIT_PRIORITY = 0,
    localparam int C_LOG2_PORTS = $clog2(P_PORT_N)
) (
    input  logic                      rst_n_i,
    input  logic                      clk_i,
    input  logic     [P_PORT_N - 1:0] request_i,
    output logic     [P_PORT_N - 1:0] grant_o,
    output logic [C_LOG2_PORTS - 1:0] select_o,  // selected in previous round port
    output logic                      valid_o,   // selected port had valid request
    input  logic                      ack_i      // acknowledge to select_o/valid_o
);
    localparam int C_INIT_SELECT = P_INIT_PRIORITY ? P_INIT_PRIORITY - 1 : P_PORT_N - 1;

    wire  handshake;
    wire  round_change;
    wire  output_update;
    wire  next_valid;
    logic [C_LOG2_PORTS - 1:0] round;               // port with highest priority
    logic [C_LOG2_PORTS - 1:0] next_round;
    logic [C_LOG2_PORTS - 1:0] next_port;           // round winner 
    logic [P_LOG2_MAX_WEIGHT_P1 - 1:0] weight_cnt;  // current round weight 
    logic     [P_PORT_N - 1:0] grant_int;

    always_ff @(posedge clk_i) begin : p_round
        if (rst_n_i == 0)
            round <= P_INIT_PRIORITY;
        else if (round_change)
            round <= next_round;
    end

    always_ff @(posedge clk_i) begin : p_weight_counter
        if (rst_n_i == 0)
            weight_cnt <= 0;
        else begin
            if (round_change)
                weight_cnt <= 0;            
            else
                if (output_update && next_valid)
                    weight_cnt <= weight_cnt + 1;
        end
    end

    always_ff @(posedge clk_i) begin : p_select_valid
        if (rst_n_i == 0) begin
            select_o <= C_INIT_SELECT;
            valid_o <= 1'b0;
        end
        else
            if (output_update) begin
                select_o <= next_port;
                valid_o <= next_valid;
            end
    end

    always_comb begin : p_next_port
        for (int i = 0; i < P_PORT_N; ++i) begin
            next_port = ((round + i) < P_PORT_N) ? round + i : round + i - P_PORT_N;
            if (request_i[next_port] == 1'b1)
                break;
        end
        grant_int = '0;
        grant_int[next_port] = output_update;  // can lead to grant without request
    end

    always_comb begin : p_comb
        next_round <= (next_port == (P_PORT_N - 1)) ? 0 : next_port + 1;
        grant_o <= grant_int & request_i;  // mask grant without corresponding request
    end

    assign next_valid = |request_i;
    assign handshake = valid_o && ack_i;
    assign output_update = (!valid_o) || ack_i;
    assign round_change = output_update && (P_WEIGHTS[next_port] == weight_cnt);

endmodule
