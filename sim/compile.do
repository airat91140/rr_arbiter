do clean.do

source param.do

vlib work

if {$code_coverage} {
    quietly set cov_param "+cover=bcsf"
} else {
    quietly set cov_param "+nocover"
}

#compile DUT
vlog -sv -timescale $ts_param -f $rtl_dir/source.list $cov_param +libext+.v+.vlib 

#compile TB environment
vlog -timescale $ts_param -L mtiUvm $cov_param +incdir+$tb_dir+$proj_home -f $tb_dir/tb.list

