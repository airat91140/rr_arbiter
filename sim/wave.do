onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider AXI
add wave -noupdate /top/inf/*
add wave -noupdate /top/arb/*
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 3} {11778620 ps} 0} {{Cursor 2} {75734050 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 329
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 25000
configure wave -griddelta 10
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {106738820 ps}
