/****************************************************************************
 * rr_driver.sv
 ****************************************************************************/
`ifndef RR_DRIVER__SV
`define RR_DRIVER__SV

`include "rr_item.sv"

/**
 * Class: rr_write_driver
 *
 * driver class for creating stimulus to arbiter
 */
class rr_driver #(parameter P_PORT_N = 4) extends uvm_driver #(rr_item#(P_PORT_N));
    rr_item #(P_PORT_N) item;

    virtual rr_arb_if #(P_PORT_N).TEST vif;

    `uvm_component_param_utils(rr_tb_pkg::rr_driver#(P_PORT_N))

    function new(string name = "rr_driver", uvm_component parent);
        super.new(name, parent);
    endfunction : new

    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        if(!uvm_config_db#(virtual rr_arb_if #(P_PORT_N))::get(this, "", "vif_test", vif))
            `uvm_fatal("NOVIF", {"virtual interface must be set for: ", get_full_name(),".vif_test"});
    endfunction : build_phase

    task run_phase(uvm_phase phase);
        vif.request_i = '0;
        vif.ack_i = 1'b0;
        vif.rst_n_i = 1'b1;
        forever begin
            @(posedge vif.clk_i);
            seq_item_port.get_next_item(item);
            drive_item(item);
            if (rr_tb_cfg::driver_log)
                `uvm_info("item driven", $sformatf("request %b, ack: %b", vif.request_i, vif.ack_i), UVM_MEDIUM)
            seq_item_port.item_done();
        end
    endtask : run_phase

    task drive_item(rr_item#(P_PORT_N) item);
        vif.request_i = item.request;
        vif.ack_i = item.ack;
        vif.rst_n_i = item.rst;
    endtask : drive_item

endclass : rr_driver

`endif // RR_DRIVER__SV
