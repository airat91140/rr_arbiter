`ifndef RR_TYPES__SV
`define RR_TYPES__SV

typedef enum {INIT, RELEASE_REQ, REQUEST, ACK} seq_state_e;

`endif // RR_TYPES__SV
