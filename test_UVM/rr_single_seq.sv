/****************************************************************************
 * rr_single_seq.sv
 ****************************************************************************/

`ifndef RR_SINGLE_SEQ__SV
`define RR_SINGLE_SEQ__SV

/**
 * Class: rr_single_seq
 * 
 * sequence class for one operation
 */
class rr_single_seq #(
        parameter P_DEF_WEIGHT = 0,
        parameter P_PORT_N = 4,
        parameter int P_WEIGHTS[P_PORT_N] = '{default: P_DEF_WEIGHT},
        parameter P_LOG2_MAX_WEIGHT_P1 = $clog2(P_DEF_WEIGHT + 1),
        parameter P_INIT_PRIORITY = 0
    ) extends uvm_sequence#(rr_item#(P_PORT_N));

    bit [P_PORT_N - 1 : 0] request;
    bit ack;
    bit rst;
    event cycle;	
    
    `uvm_object_param_utils(rr_tb_pkg::rr_single_seq #(
        .P_DEF_WEIGHT(P_DEF_WEIGHT),
        .P_PORT_N(P_PORT_N),
        .P_INIT_PRIORITY(P_INIT_PRIORITY),
        .P_WEIGHTS(P_WEIGHTS),
        .P_LOG2_MAX_WEIGHT_P1(P_LOG2_MAX_WEIGHT_P1)
        )
    )
    
    virtual function void solve_granted();
        return;	
    endfunction : solve_granted
    
    virtual task send_transaction;
        req = rr_item#(P_PORT_N)::type_id::create("item");
        start_item(req);
        req.request = request;
        req.ack = ack;
        req.rst = rst;
        ack = 1'b0;
        rst = 1'b1;
        solve_granted();
        ->cycle;
        finish_item(req);
        if (rr_tb_cfg::sequence_log)
            `uvm_info("item sent", $sformatf("request: %b, ack: %b", req.request, req.ack), UVM_MEDIUM)
    endtask : send_transaction	
    
    virtual task body();
        send_transaction;
    endtask : body
    
    function new(string name = "single_seq");
        super.new(name);
    endfunction : new

endclass : rr_single_seq

`endif // RR_SINGLE_SEQ__SV
