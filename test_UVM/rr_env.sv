/****************************************************************************
 * rr_env.sv
 ****************************************************************************/
`ifndef RR_ENV__SV
`define RR_ENV__SV

`include "rr_agent.sv"
`include "rr_scoreboard.sv"

/**
 * Class: rr_env
 *
 * top environment class
 */
class rr_env #(
        int P_PORT_N = 4,
        int P_DEF_WEIGHT = 0,
        int P_WEIGHTS[P_PORT_N] = '{default: P_DEF_WEIGHT},
        int P_LOG2_MAX_WEIGHT_P1 = $clog2(P_DEF_WEIGHT + 1),
        int P_INIT_PRIORITY = 0
    ) extends uvm_env;
    
    rr_agent #(.P_PORT_N(P_PORT_N)) agent;
    
    rr_scoreboard #(
        .P_DEF_WEIGHT(P_DEF_WEIGHT),
        .P_PORT_N(P_PORT_N),
        .P_INIT_PRIORITY(P_INIT_PRIORITY),
        .P_WEIGHTS(P_WEIGHTS),
        .P_LOG2_MAX_WEIGHT_P1(P_LOG2_MAX_WEIGHT_P1)
    ) scb;

    `uvm_component_param_utils(rr_tb_pkg::rr_env#(
            .P_DEF_WEIGHT(P_DEF_WEIGHT),
            .P_PORT_N(P_PORT_N),
            .P_INIT_PRIORITY(P_INIT_PRIORITY),
            .P_WEIGHTS(P_WEIGHTS),
            .P_LOG2_MAX_WEIGHT_P1(P_LOG2_MAX_WEIGHT_P1)
        )
    )

    function new(string name = "rr_env", uvm_component parent);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        
        agent = rr_agent #(.P_PORT_N(P_PORT_N))::type_id::create("agent", this);
        
        scb = rr_scoreboard #(
                .P_DEF_WEIGHT(P_DEF_WEIGHT),
                .P_PORT_N(P_PORT_N),
                .P_INIT_PRIORITY(P_INIT_PRIORITY),
                .P_WEIGHTS(P_WEIGHTS),
                .P_LOG2_MAX_WEIGHT_P1(P_LOG2_MAX_WEIGHT_P1)
            )::type_id::create("scoreboard", this);
    endfunction : build_phase

    virtual function void connect_phase(uvm_phase phase);
        agent.monitor.item_collected_port.connect(scb.item_collected_export);
    endfunction : connect_phase

endclass : rr_env

`endif // RR_ENV__SV
