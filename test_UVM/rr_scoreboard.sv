/****************************************************************************
 * rr_scoreboard.sv
 ****************************************************************************/
`ifndef RR_SCOREBOARD__SV
`define RR_SCOREBOARD__SV

import uvm_pkg::*;
`include "uvm_macros.svh"

`include "rr_transfer.sv"

/**
 * Class: rr_scoreboard
 *
 * scoreboard class
 */
class rr_scoreboard #(
        int P_PORT_N = 4,
        int P_DEF_WEIGHT = 0,
        int P_WEIGHTS[P_PORT_N] = '{default: P_DEF_WEIGHT},
        int P_LOG2_MAX_WEIGHT_P1 = $clog2(P_DEF_WEIGHT + 1),
        int P_INIT_PRIORITY = 0
    ) extends uvm_scoreboard;
    
    localparam int C_INIT_SELECT = P_INIT_PRIORITY ? P_INIT_PRIORITY - 1 : P_PORT_N - 1;

    typedef bit [P_PORT_N - 1 : 0] req_arr;
    int sbd_error = 0;
    bit [P_PORT_N - 1 : 0] exp_grant;
    bit new_request = 1'b1;
    bit granted = 1'b0;
    int weight_counter;
    bit [P_PORT_N - 1 : 0] mask = '1 << P_INIT_PRIORITY;

    uvm_analysis_imp #(
        rr_transfer#(P_PORT_N), 
        rr_scoreboard#(
            .P_DEF_WEIGHT(P_DEF_WEIGHT),
            .P_PORT_N(P_PORT_N),
            .P_INIT_PRIORITY(P_INIT_PRIORITY),
            .P_WEIGHTS(P_WEIGHTS),
            .P_LOG2_MAX_WEIGHT_P1(P_LOG2_MAX_WEIGHT_P1)
        )
    ) item_collected_export;

    `uvm_component_param_utils(rr_tb_pkg::rr_scoreboard #(
            .P_DEF_WEIGHT(P_DEF_WEIGHT),
            .P_PORT_N(P_PORT_N),
            .P_INIT_PRIORITY(P_INIT_PRIORITY),
            .P_WEIGHTS(P_WEIGHTS),
            .P_LOG2_MAX_WEIGHT_P1(P_LOG2_MAX_WEIGHT_P1)
        )
    )

    function new(string name = "rr_scoreboard", uvm_component parent = null);
        super.new(name, parent);
    endfunction : new

    function void build_phase(uvm_phase phase);
        item_collected_export = new("item_collected_export", this);
    endfunction	: build_phase

    virtual function void write(rr_transfer #(P_PORT_N) trans);
        if (rr_tb_cfg::scoreboard_log)
            `uvm_info("trans recieved ", $sformatf("request: %b, grant: %b, select: %0d, ack: %b, valid: %b, rst: %b",
                                            trans.request, trans.grant, trans.select, trans.ack, trans.valid, trans.rst), UVM_MEDIUM);
        check_data(trans);
    endfunction : write
            
    function void check_rst(rr_transfer #(P_PORT_N) trans);
        logic rst = 1'b1;
        if (!rst) begin
            if (trans.valid == 1'b1 || trans.select != C_INIT_SELECT) begin
                `uvm_error("RSTERR", $sformatf("incorrect reset reaction: valid: expected %b, recieved %b, select: expected %0d, recieved %0d", 
                        1'b0, trans.valid, C_INIT_SELECT, trans.select));
                sbd_error++;
            end
            rst = 1'b1;
            weight_counter = 0;
            mask = '1 << P_INIT_PRIORITY;
        end
    if (!trans.rst)
        rst = trans.rst;    	
    endfunction : check_rst

    function void check_egress(rr_transfer #(P_PORT_N) trans);
        if (granted) begin
            granted = 1'b0;
            if ((1 << trans.select) != exp_grant) begin
                sbd_error++;
                `uvm_error("SELERR", $sformatf("incorrect select: expected %0d, recieved %0d", $clog2(exp_grant), trans.select)); // check if select corresponds with the grant  		
            end
            if (!trans.valid) begin
                sbd_error++;
                `uvm_error("VALERR", "incorrect valid: expected 1'b1, recieved 1'b0"); // check that valid flag is set      		
            end
        end    	
    endfunction : check_egress
            
    function void check_grant(rr_transfer #(P_PORT_N) trans);
        if (!trans.valid || trans.ack) begin
            exp_grant = get_expected_grant(trans);
            granted = 1'b1;
        end
        if (granted && (trans.grant != exp_grant)) begin
            sbd_error++;
            `uvm_error("GRANTERR", $sformatf("incorrect grant: expected %b, recieved %b", exp_grant, trans.grant)); // check grant correctness
        end    	
    endfunction : check_grant
    
    function void weight_counter_control(rr_transfer #(P_PORT_N) trans);
        if (trans.ack && trans.valid) begin
            if (weight_counter++ == P_WEIGHTS[$clog2(mask & ~(mask - 1))]) begin
                weight_counter = 0;
                mask = mask << 1 ? mask << 1 : '1;        		
            end	        	
        new_request = 1'b1; // new round
    end    	
    endfunction : weight_counter_control
    
    function void check_data(rr_transfer #(P_PORT_N) trans);
        check_rst(trans);
        if (new_request && !trans.request)
            return;
        new_request = 1'b0;
        check_egress(trans);
        weight_counter_control(trans);    	
        check_grant(trans);
    endfunction : check_data

    function req_arr get_expected_grant(rr_transfer #(P_PORT_N) trans);
        bit [P_PORT_N - 1 : 0] result;
        result = trans.request & mask & ~((trans.request & mask) - 1);
        if (!result)
            result = trans.request & ~(trans.request - 1);
        mask = '0;
        for (int i = 0; i < P_PORT_N; ++i) begin // fill mask with all 1 from the position granted request
            mask |= result << i;
        end      
        return result;
    endfunction : get_expected_grant

endclass : rr_scoreboard

`endif // RR_SCOREBOARD__SV
