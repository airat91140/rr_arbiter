/****************************************************************************
 * rr_agent.sv
 ****************************************************************************/
`ifndef RR_AGENT__SV
`define RR_AGENT__SV

`include "rr_driver.sv"
`include "rr_monitor.sv"
`include "rr_item.sv"

/**
 * Class: rr_agent
 *
 * class for implementing agent functions
 */
class rr_agent #(parameter P_PORT_N = 4) extends uvm_agent;
    uvm_sequencer #(rr_item#(P_PORT_N)) sequencer;
    rr_driver #(P_PORT_N) driver;
    rr_monitor #(P_PORT_N) monitor;

    `uvm_component_param_utils(rr_tb_pkg::rr_agent#(P_PORT_N))

    function new(string name = "rr_agent", uvm_component parent);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        monitor = rr_monitor#(P_PORT_N)::type_id::create("monitor", this);
        if (is_active == UVM_ACTIVE) begin
            sequencer = uvm_sequencer#(rr_item#(P_PORT_N))::type_id::create("sequencer", this);
            driver = rr_driver#(P_PORT_N)::type_id::create("driver", this);
        end
    endfunction : build_phase

    virtual function void connect_phase(uvm_phase phase);
        if (is_active == UVM_ACTIVE)
            driver.seq_item_port.connect(sequencer.seq_item_export);
    endfunction : connect_phase

endclass : rr_agent

`endif  // RR_AGENT__SV
