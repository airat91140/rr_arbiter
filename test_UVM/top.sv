`ifndef FIFO_TOP__SV
`define FIFO_TOP__SV

`timescale 1ns/1ns
`define P_PORT_N 10
`define P_INIT_PRIORITY 5
`define P_WEIGHTS '{4, 0, 2, 2, 3, 1, 4, 3, 5, 0}
`define P_LOG2_MAX_WEIGHT_P1 3

import uvm_pkg::*;
`include "uvm_macros.svh"

`include "rr_tb_pkg.sv"
import rr_tb_pkg::*;

`include "rr_test_lib.sv"

module top;
    parameter P_PORT_N = `P_PORT_N;

    logic clk;
    logic rst;

    rr_arb_if #(.P_PORT_N(P_PORT_N)) inf (.clk_i(clk));

    virtual rr_arb_if #(.P_PORT_N(P_PORT_N)) vif;

    initial begin
        clk = 0;
        forever #5ns clk = ~clk;
    end

    initial begin
        uvm_root root;
        vif = inf;
        root = uvm_root::get();
        uvm_config_db#(virtual rr_arb_if#(P_PORT_N))::set(root, "*", "vif_test", vif);
        uvm_config_db#(virtual rr_arb_if#(P_PORT_N))::set(root, "*", "vif_monitor", vif);
        run_test();
    end

    rr_arbiter #(
        .P_PORT_N(`P_PORT_N),
        .P_INIT_PRIORITY(`P_INIT_PRIORITY),
        .P_WEIGHTS(`P_WEIGHTS),
        .P_LOG2_MAX_WEIGHT_P1(`P_LOG2_MAX_WEIGHT_P1)
    ) arb (
        .rst_n_i(inf.rst_n_i),
        .clk_i(inf.clk_i),
        .request_i(inf.request_i),
        .grant_o(inf.grant_o),
        .select_o(inf.select_o),
        .valid_o(inf.valid_o),
        .ack_i(inf.ack_i) 
    );

endmodule : top

`endif // FIFO_TOP__SV

