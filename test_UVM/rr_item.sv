/****************************************************************************
 * rr_item.sv
 ****************************************************************************/
`ifndef RR_ITEM__SV
`define RR_ITEM__SV

import uvm_pkg::*;
`include "uvm_macros.svh"

/**
 * Class: rr_item
 *
 * class that provides implementation of transaction from sequencer to driver
 */
class rr_item #(parameter P_PORT_N = 4) extends uvm_sequence_item;
    bit rst;
    bit [P_PORT_N - 1 : 0] request;
    bit ack;

    `uvm_object_param_utils(rr_tb_pkg::rr_item#(P_PORT_N))

    function new(string name = "rr_item");
        super.new(name);
    endfunction : new

endclass : rr_item

`endif // RR_ITEM__SV
