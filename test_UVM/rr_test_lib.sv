/****************************************************************************
 * test_lib.sv
 ****************************************************************************/
`ifndef TEST_LIB__SV
`define TEST_LIB__SV

import uvm_pkg::*;
`include "uvm_macros.svh"

`include "rr_base_test.sv"
`include "rr_crt_seq.sv"

/**
 * Class: crt_test
 *
 * simple crt test
 */
class crt_test extends rr_base_test;

    `uvm_component_utils(crt_test)

    function new(string name = "crt_test", uvm_component parent = null);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.seq = rr_crt_seq #(
            .P_PORT_N(super.P_PORT_N),
            .P_INIT_PRIORITY(super.P_INIT_PRIORITY),
            .P_WEIGHTS(super.P_WEIGHTS),
            .P_LOG2_MAX_WEIGHT_P1(super.P_LOG2_MAX_WEIGHT_P1)
        )::type_id::create("seq");
        super.build_phase(phase);
    endfunction : build_phase

    virtual task run_phase(uvm_phase phase);
        phase.raise_objection(this);
        seq.start(env.agent.sequencer);
        phase.drop_objection(this);
    endtask : run_phase
    
endclass : crt_test

class direct_test extends rr_base_test;

    `uvm_component_utils(direct_test)

    function new(string name = "direct_test", uvm_component parent = null);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.seq = rr_single_seq #(
                .P_PORT_N(super.P_PORT_N),
                .P_INIT_PRIORITY(super.P_INIT_PRIORITY),
                .P_WEIGHTS(super.P_WEIGHTS),
                .P_LOG2_MAX_WEIGHT_P1(super.P_LOG2_MAX_WEIGHT_P1)
            )::type_id::create("seq");
        super.build_phase(phase);
    endfunction : build_phase
    
    virtual task run_phase(uvm_phase phase);
        phase.raise_objection(this);
        seq.rst = 1'b0;
        seq.request = '0;
        seq.ack = 1'b0;
        seq.start(env.agent.sequencer);
        seq.rst = 1'b1;
        seq.request = 2'b01;
        seq.ack = 1'b0;
        seq.start(env.agent.sequencer);
        seq.request = 2'b01;
        seq.ack = 1'b1;
        seq.start(env.agent.sequencer);
        seq.request = 2'b00;
        seq.ack = 1'b1;
        seq.start(env.agent.sequencer);
        seq.request = 2'b10;
        seq.ack = 1'b0;
        seq.start(env.agent.sequencer);
        seq.request = 2'b10;
        seq.ack = 1'b1;
        seq.start(env.agent.sequencer);
        seq.request = 2'b10;
        seq.ack = 1'b1;
        seq.start(env.agent.sequencer);
        seq.request = 2'b10;
        seq.ack = 1'b1;
        seq.start(env.agent.sequencer);	
        seq.request = 2'b10;
        seq.ack = 1'b1;
        seq.start(env.agent.sequencer);	
        seq.request = 2'b10;
        seq.ack = 1'b1;
        seq.start(env.agent.sequencer);	
        seq.request = 2'b10;
        seq.ack = 1'b1;
        seq.start(env.agent.sequencer);			
        phase.drop_objection(this);
    endtask : run_phase	

endclass : direct_test

`endif  //  TEST_LIB__SV
