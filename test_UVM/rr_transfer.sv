/****************************************************************************
 * rr_transfer.sv
 ****************************************************************************/
`ifndef RR_TRANSFER__SV
`define RR_TRANSFER__SV

import uvm_pkg::*;
`include "uvm_macros.svh"

/**
 * Class: rr_transfer
 *
 * packet that goes from DUT to scoreboard
 */
class rr_transfer #(parameter P_PORT_N = 4) extends uvm_object;
    logic valid, ack, rst;
    logic [P_PORT_N - 1 : 0] request, grant;
    logic [$clog2(P_PORT_N) - 1 : 0] select;


    `uvm_object_param_utils(rr_tb_pkg::rr_transfer#(P_PORT_N));

    function new(string name = "rr_transfer");
        super.new(name);
    endfunction : new

    function void set(
        logic                            valid, 
        logic                            ack,
        logic                            rst,
        logic [P_PORT_N - 1 : 0]         request, 
        logic [P_PORT_N - 1 : 0]         grant,
        logic [$clog2(P_PORT_N) - 1 : 0] select
    );
        this.valid = valid;
        this.ack = ack;
        this.request = request;
        this.grant = grant;
        this.rst = rst;
        this.select = select;
    endfunction : set

endclass : rr_transfer

`endif // RR_TRANSFER__SV
