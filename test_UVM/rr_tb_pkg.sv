`ifndef RR_TB_PKG__SV
`define RR_TB_PKG__SV

package rr_tb_pkg;

import uvm_pkg::*;
`include "uvm_macros.svh"

`include "rr_tb_cfg.sv"
`include "rr_crt_seq.sv"
`include "rr_env.sv"

endpackage : rr_tb_pkg

`endif // RR_TB_PKG__SV
