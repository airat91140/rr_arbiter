/****************************************************************************
 * rr_base_test.sv
 ****************************************************************************/
`ifndef RR_BASE_TEST__SV
`define RR_BASE_TEST__SV

`include "rr_env.sv"
`include "rr_crt_seq.sv"

/**
 * Class: rr_base_test
 *
 * base class for all tests
 */
class rr_base_test extends uvm_test;
    parameter int P_PORT_N = `P_PORT_N;
    parameter int P_WEIGHTS[P_PORT_N] = `P_WEIGHTS;
    parameter int P_LOG2_MAX_WEIGHT_P1 = `P_LOG2_MAX_WEIGHT_P1;
    parameter int P_INIT_PRIORITY = `P_INIT_PRIORITY;

    `uvm_component_utils(rr_base_test)

    rr_env #(
            .P_PORT_N(P_PORT_N),
            .P_INIT_PRIORITY(P_INIT_PRIORITY),
            .P_WEIGHTS(P_WEIGHTS),
            .P_LOG2_MAX_WEIGHT_P1(P_LOG2_MAX_WEIGHT_P1)
    ) env;
    
    rr_single_seq #(
            .P_PORT_N(P_PORT_N),
            .P_INIT_PRIORITY(P_INIT_PRIORITY),
            .P_WEIGHTS(P_WEIGHTS),
            .P_LOG2_MAX_WEIGHT_P1(P_LOG2_MAX_WEIGHT_P1)
    ) seq;

    uvm_table_printer printer;
    bit test_pass = 1;

    function new(string name = "rr_base_test", uvm_component parent = null);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        uvm_config_db#(int)::set(this, "*", "recording_detail", UVM_FULL);
        env = rr_env #(
                    .P_PORT_N(P_PORT_N),
                    .P_INIT_PRIORITY(P_INIT_PRIORITY),
                    .P_WEIGHTS(P_WEIGHTS),
                    .P_LOG2_MAX_WEIGHT_P1(P_LOG2_MAX_WEIGHT_P1)
            )::type_id::create("env", this);
        seq.randomize();
        printer = new();
        printer.knobs.depth = 3;
    endfunction	: build_phase

    function void extract_phase(uvm_phase phase);
        if(env.scb.sbd_error)
            test_pass = 1'b0;
    endfunction	: extract_phase

    function void report_phase(uvm_phase phase);
        if(test_pass)
            `uvm_info(get_type_name(), "** UVM TEST PASSED **", UVM_NONE)
        else
            `uvm_error(get_type_name(), "** UVM TEST FAIL **")
    endfunction	: report_phase

endclass : rr_base_test

`endif  // RR_BASE_TEST__SV
