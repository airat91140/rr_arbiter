`ifndef RR_TB_CFG__SV
`define RR_TB_CFG__SV

class rr_tb_cfg extends uvm_object;
    static bit driver_log = 0;
    static bit monitor_log = 0;
    static bit scoreboard_log = 1;
    static bit sequence_log = 0;
    static bit coverage_on = 0;
endclass : rr_tb_cfg

`endif // RR_TB_CFG__SV
