/****************************************************************************
 * fifo_write_seq.sv
 ****************************************************************************/
`ifndef RR_CRT_SEQ__SV
`define RR_CRT_SEQ__SV

`include "rr_item.sv"
`include "rr_types.sv"
`include "rr_single_seq.sv"

/**
 * Class: rr_crt_seq
 *
 * sequence crt class for arbiter
 */
class rr_crt_seq #(
    parameter P_DEF_WEIGHT = 0,
    parameter P_PORT_N = 4,
    parameter int P_WEIGHTS[P_PORT_N] = '{default: P_DEF_WEIGHT},
    parameter P_LOG2_MAX_WEIGHT_P1 = $clog2(P_DEF_WEIGHT + 1),
    parameter P_INIT_PRIORITY = 0
) extends rr_single_seq #(
    .P_PORT_N(P_PORT_N),
    .P_INIT_PRIORITY(P_INIT_PRIORITY),
    .P_WEIGHTS(P_WEIGHTS),
    .P_LOG2_MAX_WEIGHT_P1(P_LOG2_MAX_WEIGHT_P1)
);
    
    rand integer count;
    bit solved;
    bit [P_LOG2_MAX_WEIGHT_P1 - 1 : 0] weights [P_PORT_N];
    bit [$clog2(P_PORT_N) - 1 : 0] round;
    bit [$clog2(P_PORT_N) - 1 : 0] granted;
    seq_state_e state[P_PORT_N];
    semaphore mutex;   

    constraint count_c {
        count inside {[5000 : 10000]};
    }

    rr_item #(P_PORT_N) req;

    function new(string name = "rr_seq");
        super.new(name);
        for (int  i = 0; i < P_PORT_N; ++i) begin
            state[i] = INIT;
            weights[i] = P_WEIGHTS[i];
        end
        mutex = new(1);
        round = P_INIT_PRIORITY;
        solved = 0;
    endfunction : new

    `uvm_object_param_utils(rr_tb_pkg::rr_crt_seq #(
            .P_DEF_WEIGHT(P_DEF_WEIGHT),
            .P_PORT_N(P_PORT_N),
            .P_INIT_PRIORITY(P_INIT_PRIORITY),
            .P_WEIGHTS(P_WEIGHTS),
            .P_LOG2_MAX_WEIGHT_P1(P_LOG2_MAX_WEIGHT_P1)
        )
    )

    virtual task body();
        fork
            run_sequences;
            forever send_transaction;
        join_any
        disable fork;
    endtask : body

    virtual task put_rst();
        rst = 1'b0;
    endtask : put_rst

    virtual task run_sequences;
        put_rst();
        @(cycle);
        for (integer i = 0; i < P_PORT_N; ++i) begin
            fork
                automatic integer copy_i = i;
                while (count > 0) begin
                    @(cycle);
                    automata_iter(copy_i);
                end
            join_none
        end
        wait fork;
        @(cycle);
    endtask : run_sequences

    task automata_iter(integer i);
        if (rr_tb_cfg::sequence_log)
            `uvm_info("Automata state was", $sformatf("port: %d: state: %s", i, state[i]), UVM_MEDIUM)
        case (state[i])
            INIT: begin // с вероятностью переходят или в состояние REQUEST или остаются в INIT
                do_init(i);
            end
            REQUEST: begin // выставляется соответствующему порту 1, переходит в ack
                do_request(i);
            end
            ACK: begin // ожидает получение гранта, переходит или в себя, или в RELEASE_REQ с определнной вероятностью
                do_ack(i);
            end
            RELEASE_REQ: begin // меняет раунд, снимает запрос, переходит в INIT
                do_release_req(i);
            end
        endcase
        if (rr_tb_cfg::sequence_log)
            `uvm_info("Automata state is", $sformatf("port: %d: state: %s", i, state[i]), UVM_MEDIUM)
    endtask : automata_iter

    task do_init(integer i);
        weights[i] = P_WEIGHTS[i];
        randcase
            85: state[i] = INIT;
            15: state[i] = REQUEST;
        endcase
    endtask : do_init

    task do_request(integer i);
        set_req(i);
        state[i] = ACK;
    endtask : do_request

    task do_ack(integer i);
        wait_grant(i);            	
        randcase
            70: begin
                set_ack();
                if (weights[i]-- == 0)
                    state[i] = RELEASE_REQ;
                else
                    state[i] = ACK;
                end
            30: begin
                state[i] = ACK;
            end
        endcase
    endtask : do_ack

    task do_release_req(integer i);
        clear_req(i);                        	
        change_round();
        state[i] = INIT;
    endtask : do_release_req

    virtual function void solve_granted();
        if (solved || !request)
            return;
        for (int i = 0; i < P_PORT_N; ++i) begin
            granted = ((round + i) < P_PORT_N) ? round + i : round + i - P_PORT_N;
            if (request[granted] == 1'b1)
                break;
        end
        solved = 1'b1;
    endfunction : solve_granted

    function void change_round();
        round = (granted + 1) % P_PORT_N;
        solved = 1'b0;
    endfunction : change_round 

    task set_req(integer i);
        request[i] = 1'b1;
    endtask : set_req

    task clear_req(integer i);
        request[i] = 1'b0;
    endtask : clear_req

    task wait_grant(integer i);
        while (granted != i && count > 0)
            @(cycle);
    endtask : wait_grant

    task set_ack();
        ack = 1'b1;
        mutex.get(1);
        --count;
        mutex.put(1);
    endtask : set_ack

endclass : rr_crt_seq

`endif // RR_CRT_SEQ__SV
