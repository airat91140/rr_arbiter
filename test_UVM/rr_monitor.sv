/****************************************************************************
 * rr_monitor.sv
 ****************************************************************************/
`ifndef RR_MONITOR__SV
`define RR_MONITOR__SV

`include "rr_item.sv"
`include "rr_transfer.sv"

/**
 * Class: rr_monitor
 *
 * monitor class for accessing outputs of the DUT
 */
class rr_monitor #(parameter P_PORT_N = 4) extends uvm_monitor;
    bit [P_PORT_N - 1 : 0] grant_cg_values [P_PORT_N];

    virtual rr_arb_if #(P_PORT_N).MONITOR vif;

    uvm_analysis_port #(rr_transfer#(P_PORT_N)) item_collected_port;

    rr_transfer#(P_PORT_N) trans;

    `uvm_component_param_utils(rr_tb_pkg::rr_monitor#(P_PORT_N))

    covergroup cg;
        request: coverpoint trans.request;
        grant: coverpoint trans.grant {
            bins gr[P_PORT_N] = grant_cg_values;
        }
    endgroup

    function new(string name, uvm_component parent);
        super.new(name, parent);
        item_collected_port = new("item_collected_port", this);
        if (rr_tb_cfg::coverage_on) begin
            for (int i = 0; i < P_PORT_N; ++i) begin
                grant_cg_values[i] = ('0 | 1'b1) << i;
            end
            cg = new();
        end
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        if(!uvm_config_db#(virtual rr_arb_if#(P_PORT_N))::get(this, "", "vif_monitor", vif))
            `uvm_fatal("NOVIF", {"virtual interface must be set for: ", get_full_name(),".vif_monitor"});
    endfunction : build_phase

    virtual task run_phase(uvm_phase phase);
        collect_transactions();
    endtask : run_phase

    function bit compare(rr_transfer#(P_PORT_N) trans);
        return trans.valid == vif.valid_o && 
               trans.ack == vif.ack_i && trans.ack != 1'b1 &&
               trans.request == vif.request_i &&
               trans.grant == vif.grant_o &&
               trans.rst == vif.rst_n_i &&
               trans.select == vif.select_o;
    endfunction : compare

    virtual protected task collect_transactions();
        rr_transfer #(P_PORT_N) old_trans;
        old_trans = new();
        old_trans.set(.valid(1'b0), .ack(1'b0), .request('0), .grant('0), .rst(1'b0), .select('0));
        @(posedge vif.rst_n_i);
        forever begin
            @(posedge vif.clk_i);
            if (compare(old_trans) && !rr_tb_cfg::coverage_on)
                continue;
            trans = rr_transfer#(P_PORT_N)::type_id::create("rr_transfer");
            trans.set(
                .valid(vif.valid_o), 
                .ack(vif.ack_i), 
                .rst(vif.rst_n_i), 
                .request(vif.request_i),
                .grant(vif.grant_o), 
                .select(vif.select_o)
            );
            if (rr_tb_cfg::coverage_on)
                perform_transfer_covegerage();
            if (compare(old_trans))
                continue;
            old_trans = trans;
            item_collected_port.write(trans);
            if (rr_tb_cfg::monitor_log)
                `uvm_info("trans sent", $sformatf("request: %b, grant: %b, ack: %b, valid: %b", 
                                                trans.request, trans.grant, trans.ack, trans.valid), UVM_MEDIUM)
        end
    endtask : collect_transactions

    virtual function void perform_transfer_covegerage();
        cg.sample();
    endfunction : perform_transfer_covegerage

endclass : rr_monitor

`endif // RR_MONITOR__SV
